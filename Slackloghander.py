﻿import psycopg2
from slackclient import SlackClient
from datetime import datetime, date

def format_builtin(n):
	if n:
		return format(n, ',')
	return ""

class SlackService:
    def __init__(self,namebot,token):
        self.Name = namebot
        self.token = token
        self.client = SlackClient(self.token)
    def SendMessage(self,chanel,title,titlesub,mes,linkimg,fields):
        att = [{"pretext":title,
                            "fallback": "Required plain-text summary of the attachment.",
                            "color": "good",
                            "author_icon": "http://www.freeiconspng.com/uploads/simpson-happy-icon-19.png",
                            "title": titlesub,
                            "title_link": "https://api.slack.com/",
                            "text":  mes,
                            "fields":fields
                           }]
        self.client.api_call("chat.postMessage", username=self.Name, channel=chanel,attachments=att,link_names =1)
    def SendText(self,chanel,text):
        self.client.api_call("chat.postMessage", username=self.Name, channel=chanel,text=text)
    def UploadFile(self,chanel,file,filename):
        self.client.api_call("files.upload", filename=filename, channels=chanel,file=file)
		
class AppService:
    def __init__(self):
        self.connection = psycopg2.connect(host='192.168.1.3',
                                 port = '5432',
                                 user='vi21',
                                 password='Legacy@2016',
                                 dbname='vi21e-testing')
    def close(self):
        self.connection.close()
    def GetAllReport(self):
        try:
            with self.connection.cursor() as cursor:
                sql = """SELECT * from lgs_view_admin"""
                cursor.execute(sql)
                imglink = cursor.fetchall()
                print(imglink)
                return imglink
        except  ValueError:
            print "I am unable to connect to the database."
            return []
    def Insert(self,row):
        try:
            if row is not None:
                with self.connection.cursor() as cursor:
                    today = datetime.now()
                    print today.date()
                    sql = """INSERT INTO lgs_view_admin_update (sum_user,
                    sum_user_active,
                    sum_user_crate_day,
                    sum_network_id, 
                    sum_network_id_day,
                    gross_all_day,
                    sum_network,
                    date,
                    create_date,
                    write_date,
                    sum_stock_quant) VALUES (%s, %s,%s, %s,%s, %s,%s, %s,%s, %s,%s)"""
                    print sql
                    cursor.execute(sql,(row[1],row[2],row[3],row[4],row[5],row[6],row[7],row[8],today.date(),today.date(),row[9]))
                    self.connection.commit()
                print "done"
                return 1
            return 0
        except  ValueError:
            print "I am unable to insert to the lgs_view_admin_update."
            return 0


#dinh nghia cac gia tri cho slackbot
namebot = "Report"    
api_key = 'xoxb-68420109287-Zw6V0P0vOQaqtevUHF2rYF8T'
chandev = "#reportvi21e"
client = SlackService(namebot,api_key)
# Try to connect
dbcontext  = AppService()
rows = dbcontext.GetAllReport()
print rows
#slack = Slacker(api_key)
#slack.files.upload(filename,channels=chandev)
template = """
Thông tin hệ thống Vi21-E\n
```
%30s: %s
%30s: %s
%30s: %s
%30s: %s
%30s: %s
%30s: %s
```
"""
for row in rows:
    noidung = ""
    linkimg = ""
    fields = []
    fields.append({"title":"Tổng user trong hệ thống",
                   "value":str(row[1]),
                   "short": True})
    fields.append({"title":"Tổng số user active",
                   "value":str(row[2]),
                   "short": True})
    fields.append({"title":"Số user được tạo trong ngày",
                   "value":str(row[3]),
                   "short": True})
    fields.append({"title":"Tổng số điểm mạng",
                   "value":str(row[4]),
                   "short": True})
    fields.append({"title":"Nút mạng tạo ra trong ngày",
                   "value":str(row[5]),
                   "short": True})
    fields.append({"title":"Doanh thu trong ngày",
                   "value":str(format_builtin(row[6])),
                   "short": True})
    fields.append({"title":"Tổng số kho hàng có hàng",
                   "value":str(row[9]),
                   "short": True})
    tieude =""
    tieudesub ="Thông tin hệ thống Vi21-E"
    noidung = ""
    #noidung = "Tổng User Trong Hệ Thống: " + str(row[1]) + "\n"
    #noidung = noidung +  "Tổng số user active: " + str(row[2]) + "\n"
    #noidung = noidung +  "Số user được tạo trong ngày: " + str(row[3]) + "\n"
    #noidung = noidung +  "Tổng số điểm mạng: " + str(row[4]) + "\n"
    #noidung = noidung +  "Nút mạng tạo ra trong ngày: " + str(row[5]) + "\n"
    #noidung = noidung +  "Doanh Thu trong ngày: " + str(row[6]) + "\n"
    #noidung = noidung +  "Tổng số user active: " + str(row[2]) + "\n"
    #noidung = noidung +  "Tổng số user active: " + str(row[2]) + "\n"
    client.SendMessage(chandev,tieude,tieudesub,noidung,linkimg,fields)
    #dbcontext.Insert(row)
    #client.SendText(chandev,tieude)
    #fo = open("report.jpg", "rb")
    #client.UploadFile(chandev,fo,"baocao.jpg")
    print row

dbcontext.close()